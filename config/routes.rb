Rails.application.routes.draw do
  
  resources :posts
  
  root 'sessions#index'
#  get '/auth/:provider/callback' => 'sessions#create'
  get '/auth/:provider/callback', :to => 'sessions#callback'
  post '/auth/:provider/callback', :to => 'sessions#callback'
  get '/logout' => 'sessions#destroy', :as => :logout
end
